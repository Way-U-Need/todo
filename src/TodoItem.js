import React, { Component } from 'react';


class TodoItem extends Component
{
    state={
            isEditing:false,
            editText: ""
          }
    componentDidMount(){
        this.setState({editText: this.props.todo.text})
                       }
    toggleEditing=()=>{
        this.setState({isEditing:!this.state.isEditing})
                      }
    editToDo=(event)=>{
        event.preventDefault();
        const nt= this.state.editText;
        this.props.editTodoState(this.props.index,nt);
        this.toggleEditing();
    }
    render()
    {
        if(this.state.isEditing===true)
        {   
            return <form onSubmit={this.editToDo}>
                <input 
                        name="listname"
                        value={this.state.editText}
                        type="text"
                        onChange={e => this.setState({editText: e.target.value})}
                />
                <button>Save</button>
                <button onClick={this.toggleEditing}>Cancle</button>
            </form>
        }
        return(  
                <div>
                        {this.props.todo.text}
                        <button onClick={this.toggleEditing}>Edit</button>
                        <button onClick={this.props.delete}>Delete</button>
                </div>
              );
        }
}
export default TodoItem;