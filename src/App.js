import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddTodo from './AddtTodo';
import TodoItem from './TodoItem';

class App extends Component {
  state={ todo:[] }
  
  addtodoState=(newTodo)=>{
                              const {todo} = this.state;
                              this.setState({todo: todo.concat(newTodo)})
                              return todo;
                         };
  deleteList=(index)=>{
                              //console.log("DELETE clicked.!");
                              const updatedTodo = [...this.state.todo];
                              updatedTodo.splice(index,1);
                              this.setState({ todo: updatedTodo});
    
                      };
  editTodoState=(todoIndex,newText)=>{
    //utd=updated to do
                              //console.log(newText);
                              console.log('todoIndex', todoIndex)
                              const utd=this.state.todo.map((element,index)=>{
                                         if(todoIndex===index){
                                                    return{ text:newText}
                                                              }
                              return element;
                                          });
                              this.setState({todo:utd})
                                      };
  
  
  render() 
  {
      const liTags = this.state.todo.map((element,index) => (
      <TodoItem key={index} 
                delete={(e) => this.deleteList(index)} 
                todo={element} index={index}  
                editTodoState={this.editTodoState}/>
       ))
    
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
                  This is To Do project of React by Megha Brahmbhatt
        <AddTodo addtodoState={this.addtodoState} />
        <ul>
            <li>{liTags}</li>
        </ul> 
        </header>
       </div>
          );
      }
}

export default App;
