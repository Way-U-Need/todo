import React, { Component } from 'react';

class AddTodo extends Component 
{
  state={ text:"" }
  changeText= (event) =>{
                          this.setState( {text:event.target.value})
                        };   
  addTodo=(event, text)=>{
                    event.preventDefault();
                    this.props.addtodoState({text:this.state.text});
                    this.setState({text:""});
                         };
    render() {
      return (
        <div>
           <form onSubmit={this.addTodo} >
             <input onChange={this.changeText} type="text" value={this.state.text} />
                <button type="submit" >Add</button>
            </form>    
        </div>
      );
    } 
  }

  export default AddTodo;